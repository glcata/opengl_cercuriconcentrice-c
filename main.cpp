#include <GL/glut.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>


void init()
{
    glClearColor(0.0,0.0,0.0,0.0);
    glPointSize(50.0);
    glShadeModel(GL_FLAT);
}
void display()
{
    glClear(GL_COLOR_BUFFER_BIT);

    GLfloat Pi= 2*3.14;

    glBegin(GL_TRIANGLE_FAN);
    glColor3f(0,0,1);
    int sectiuni2=1000;
    GLfloat rad3=0.8;
    glVertex2f(0.0,0.0);
    for(int i=0;i<=sectiuni2;++i)
    {
        glVertex2f(rad3*cos(i*Pi/sectiuni2),rad3*sin(i*Pi/sectiuni2));
    }
    glEnd();

    glBegin(GL_TRIANGLE_FAN);
    glColor3f(1,0,0);
    int sectiuni=1000;
    GLfloat rad2=0.5;
    glVertex2f(0.0,0.0);
    for(int j=0;j<=sectiuni;++j)
    {
        glVertex2f(rad2*cos(j*Pi/sectiuni),rad2*sin(j*Pi/sectiuni));
    }
    glEnd();


    glBegin(GL_TRIANGLE_FAN);
    glColor3f(1,1,1);
    int sectiuni_i=10;
    GLfloat rad=0.1;
    glVertex2f(0.0,0.0);
    for(int i=0;i<=sectiuni_i;++i)
    {
        glVertex2f(rad*cos(i*Pi/sectiuni_i),rad*sin(i*Pi/sectiuni_i));
    }
    glEnd();

    glBegin(GL_LINES);
    glColor3f(0,1,0);
    glVertex3f(0.5,-0.5,0.0);
    glVertex3f(-0.5,-0.5,0.0);

    glVertex3f(-0.5,0.5,0.0);
    glVertex3f(0.5,0.5,0.0);

    glVertex3f(-0.5,-0.5,0.0);
    glVertex3f(-0.5,0.5,0.0);

    glVertex3f(0.5,0.5,0.0);
    glVertex3f(0.5,-0.5,0.0);

    glEnd();

    glBegin(GL_LINES);
    glColor3f(0,1,0);
    glVertex3f(0.8,-0.8,0.0);
    glVertex3f(-0.8,-0.8,0.0);

    glVertex3f(-0.8,0.8,0.0);
    glVertex3f(0.8,0.8,0.0);

    glVertex3f(-0.8,-0.8,0.0);
    glVertex3f(-0.8,0.8,0.0);

    glVertex3f(0.8,0.8,0.0);
    glVertex3f(0.8,-0.8,0.0);

    glEnd();

    glBegin(GL_LINES);
    glColor3f(0,1,0);
    glVertex3f(0.9,-0.9,0.0);
    glVertex3f(-0.9,-0.9,0.0);

    glVertex3f(-0.9,0.9,0.0);
    glVertex3f(0.9,0.9,0.0);

    glVertex3f(-0.9,-0.9,0.0);
    glVertex3f(-0.9,0.9,0.0);

    glVertex3f(0.9,0.9,0.0);
    glVertex3f(0.9,-0.9,0.0);

    glEnd();

    glFlush();
}

int main(int argc,char** argv)
{
    
    glutInit(&argc,argv);                        //este folosita pentru a initializa libraria GLUT
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);   //specifica caracterul de afisare a culorii si a buffer-ului de adancime
    glutInitWindowSize(400,400);                 //primeste ca argument inaltimea si latimea
    glutInitWindowPosition(400,100);             //primeste ca argument coordonatele
    glutCreateWindow("cercuri concentrice");     //creeaza fereastra de afisare-> aceasta se va afisa dupa apelul functiei glutMainLoop
    init();
    glutDisplayFunc(display);                    //este o functie callback(primeste ca argument o alta functie)
    glutMainLoop();                              //este o functie de executie GLUT -> permite executia unui program dupa ce au fost efectuate toate inregistrarile (bucla de executie)

    return 0;
}
